const Product = require("./../models/Products");
const auth = require("./../auth");

// 6.)
module.exports.createProduct = (reqBody, ifAdmin) => {
	
	let newProduct = new Product({
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price
	})
	
	return Product.findOne({productName: reqBody.productName}).then( (result, error) => {
		if(result != null){
			return `Product already exists`
		} else if(ifAdmin){
			return newProduct.save().then( (result, error) => {
				if(error){
					return false
				} else {
					return true
				}
			})
		} else {
			return 'not admin'
		}
	})
}

module.exports.getAllProduct = () => {

	return Product.find().then( (result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})
}


module.exports.getActiveProduct = () => {
	return Product.find({isActive: true}).then((result, err) => {
		if(err) {
			return false
		} else {
			return result
		}
	})
}


//get a specifc course
module.exports.getSpecificProduct = (params) => {
	//console.log(reqBody)	//HTML

	//look for the matching document in the database
	return Product.findbyId(params).then( (result, error) => {

		//if matching document not found, return Product not existing
		if(result == null){
			return `Product not existing`

		} else {
			//if matching document found, return the document
			if(result){
				return result
			} else {
				//if error, return error
				return error
			}
		}
	})
}

// 5.)
module.exports.getProductById = (params) => {
	
	//look for matching filter
	
	return Product.findById(params).then( (result, error) => {
		
		//if matching document not found, return Product not existing
		if(result == null){
			return `Product not existing`
		} else {
			//return matching document
			if(result){
				return result
			} else {
				//return error
				error
			}
		}
	})	
}

module.exports.archiveProduct = (reqBody) => {
	let productStatus = {
		isActive: false
	}
	
	return Product.findOneAndUpdate({productName: reqBody}, productStatus, {new:true}).then( (result, error) => {
		console.log(result)

		if(result == null){
			return `Product not existing`
		} else {
			if (result) {
				return true
			} else {
				return false
			}
		}
	})
}


module.exports.unarchiveProduct = (reqBody) => {
	let productStatus = {
		isActive: true
	}

	return Product.findOneAndUpdate({productName: reqBody.productName}, productStatus).then( (result, error) => {

		if(result == null){
			return `Product not existing`
		} else {
			if (result) {
				return true
			} else {
				return false
			}
		} 
	})
}

// 7.)
module.exports.archiveProductById = (params, ifAdmin) => {

	if(ifAdmin){
		return Product.findByIdAndUpdate(params, {isActive: false}).then( (result, error) => {

			if(result == null){
				return `Product not existing`
			} else {
				if(result){
				return true
				} else {
					return false
				}
			}
		})
	}
}


module.exports.unarchiveProductById = (params) => {

	return Product.findByIdAndUpdate(params, {isActive: true}).then( (result, error) => {

		if(result == null){
			return `Product not existing`
		} else {
			if(result){
				return true
			} else {
				return false
			}
		}
	})
}


module.exports.deleteProduct = (name) => {

	//look for matching document in the database && delete the matching document
	return Product.findOneAndDelete({productName: name}).then( (result, error) => {

		if(result == null){
			return `Product not existing`
		} else {
			if(result){
				// return true if delete 
				return true
			} else {
				// return error if error
				return error
			}
		}
	})
}


module.exports.deleteProductById = (id) => {

	return Product.findByIdAndDelete(id).then( (result, error) => {

		if(result == null){
			return `Product not existing`
		} else {
			if(result){
				return true
			} else {
				return error
			}
		}
	})
}


// 7.)
module.exports.editProduct = (id, reqBody, ifAdmin) => {
	const {productName, description, price, isActive} = reqBody

	let updatedProduct = {
		productName: productName,
		description: description,
		price: price,
		isActive: isActive
	}
	
	if(ifAdmin == false){
		return "not an admin"
	} else if(ifAdmin){
		console.log(ifAdmin)
		return Product.findByIdAndUpdate(id, updatedProduct, {new: true}).then( (result, err) => {
			if(err){
				return err
			} else {
				return result
			}
		})
	}
}


