const User = require("./../models/Users");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("./../models/Products")
const Order = require("./../models/Orders")

// 1.)
module.exports.register = (reqBody) => {

	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
	})
	
	return User.findOne({email: reqBody.email}).then( (result, error) => {
		if(result != null){
			return `User already exists`
		} else {
			return newUser.save().then( (result, error) => {
				if(result){
					return true
				} else {
					return error
				}
			})
		}
	})
}


module.exports.getAllUsers = () => {

	return User.find().then( (result, error) => {
		if(result){
			return result
		} else {
			return error
		}
	})
}

// 2.)
module.exports.login = (reqBody) => {
  const { email, password } = reqBody;
  return User.findOne({ email: email }).then((result) => {
    if (result == null) {
      return false;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(password, result.password);
      if (isPasswordCorrect == true) {
        return { access: auth.createAccessToken(result) };
      } else {
        return false;
      }
    }
  })
}


module.exports.getProfile = (user) => {
    
    return User.findById(user.id).then((result, err) => {
        if(result != null){
            result.password = "";
            return result
        } else {
            return false
        }
    })
}



// 9.)
module.exports.checkout = (data) => {
	let totalAmount = 0;

	for(i = 0; i < data.cart.length; i++){
			totalAmount += data.cart[i].price 
		}
	
	return Order.findOne({userId: data.userId}).then( (result, error) => {
	
		if(result == null){

				let newOrder = new Order({
					userId: data.userId,
					cart: [],
					totalAmount: totalAmount})

				for(i = 0; i < data.cart.length; i++){
						newOrder.cart.push({productId: data.cart[i]._id, productName: data.cart[i].productName,price: data.cart[i].price})
				}

				return newOrder.save().then( result => { return true})

		}else if(result){

				for(i = 0; i < data.cart.length; i++){
				result.cart.push({productId: data.cart[i]._id, productName: data.cart[i].productName,price: data.cart[i].price})

				}

				result.totalAmount += totalAmount

				return result.save().then( result => { return `items added`})

		}

	})		
}


// 3.)
module.exports.setAsAdmin = (params, ifAdmin) => {
	let isAdmin = {
		isAdmin: true
	}
	
	if(ifAdmin == false){
		return 'not an admin'
		
	} else {
			return User.findByIdAndUpdate(params, isAdmin, {new:true}).then( (result, error) => {
			if(result == null){
				return `User not existing`
			} else {
				if (result) {
					return true
				} else {
					return false
				}
			}
		})
	} 
}

// 10.)
module.exports.getAllOrders = (isAdmin) => {

	return Order.find().then( (result, error) => {
		if(isAdmin){
			return result
		} else {
			return `not admin`
		}
	})

}

// 11.)
module.exports.myOrders = (data) => {

		return Order.findOne({userId:data.id}).then( (result,error) => {
			if(result){
				return result
			}else{
				return false
			}
		})
}
