const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},

	cart: [
		{
			_id: false,
			productId: {
				type: String,
				
			},
			productName: {
				type: String,
				
			},
			price: {
				type: Number,
				
			}
		}
	]		
	,
	totalAmount: {
		type: Number,
		required: [true, "Product name is required"]
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	
	
})

module.exports = mongoose.model("Order", orderSchema);