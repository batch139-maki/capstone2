const express = require("express");
const router = express.Router();
const userController = require("./../controllers/userControllers");
const auth = require("./../auth");

// 1.)
router.post("/register", (req, res) => {

	userController.register(req.body).then( result => res.send(result))
})

router.get("/", (req, res) => {
  userController.getAllUsers().then( result => res.send(result))
})

// 2.)
router.get("/login", (req, res) => {
  userController.login(req.body).then( result => res.send(result))
})

// router.get("/:userId", (req, res) => {
//   let paramsId = req.params.userId
//   userController.getUserById(paramsId).then(result => res.send(result))
// })

router.get("/details", auth.verify, (req, res) => {
    let userData = auth.decode(req.headers.authorization);
    
    userController.getProfile(userData).then((result) => res.send(result))
})

// 9.)
router.post("/checkout", auth.verify, (req, res) => {
  let data = {
    userId: auth.decode(req.headers.authorization).id,
    cart: req.body
  }

  userController.checkout(data).then(result => res.send(result))
})

// 3.)
router.put("/:userId/setAsAdmin", (req, res) => {
    let userStatus = auth.decode(req.headers.authorization);
    userController.setAsAdmin(req.params.userId, userStatus.isAdmin).then((result) => res.send(result))
})

// 10.)
router.get("/orders", auth.verify, (req, res) => {
  let userStatus = auth.decode(req.headers.authorization);
  userController.getAllOrders(userStatus.isAdmin).then(result => res.send(result))
})

// 11.)
router.get("/myOrders", auth.verify, (req, res) => {
  let userStatus = auth.decode(req.headers.authorization);
  userController.myOrders(userStatus).then(result => res.send(result))
})

module.exports = router;
