const express = require("express");
const router = express.Router();

const productController = require("./../controllers/productControllers")
const auth = require("./../auth") 

//create a product 6.)
router.post("/", (req, res) => {
	let user = auth.decode(req.headers.authorization);
	productController.createProduct(req.body, user.isAdmin).then(result => res.send(result))
})
		

//retrieving all products
router.get("/allproducts", (req, res) => {
	productController.getAllProduct().then(result => res.send(result))
})


//retrieving only active products 4.)
router.get("/", (req, res) => {
	productController.getActiveProduct().then(result => res.send(result))
})

//get a specific product using findOne()
router.get("/specific-product", (req, res) => {

	// console.log(req.body)	//object

	productController.getSpecificProduct(req.body.productName).then( result => res.send(result))
})


//get specific product using findById() 5.)
router.get("/:productId", (req, res) => {

	// console.log(req.params)	//{ productId: '61979f60f63f4531cd77b395' }
	let paramsId = req.params.productId
	
	productController.getProductById(paramsId).then(result => res.send(result))
})

//update isActive status of the product using findOneAndUpdate()
	//update isActive status to false
router.put("/archive", auth.verify, (req, res) => {

	productController.archiveProduct(req.body.productName).then( result => res.send(result))
})

	//update isActive status to true 
router.put("/unarchive", auth.verify, (req, res) => {
	
	productController.unarchiveProduct(req.body).then( result => res.send(result))
})


//update isActive status of the product using findByIdAndUpdate()
	//update isActive status to false 7.)
router.put("/:productId/archive", auth.verify, (req, res) => {
	let user = auth.decode(req.headers.authorization)
	productController.archiveProductById(req.params.productId, user.isAdmin).then(result => res.send(result))
})

	//update isActive status to true
router.put("/:productId/unarchive", (req, res) => {

	productController.unarchiveProductById(req.params.productId).then( result => res.send(result))	
})

//delete product using findOneAndDelete()
router.delete("/delete-product", (req, res) => {

	productController.deleteProduct(req.body.productName).then(result => res.send(result))
})

//delete product using findByIdAndDelete()
router.delete("/:productId/delete-product", (req, res) => {

	productController.deleteProductById(req.params.productId).then(result => res.send(result))
})

// 7.)
router.put("/:productId", auth.verify, (req, res) => {
	let user = auth.decode(req.headers.authorization);
	console.log(user)
	productController.editProduct(req.params.productId, req.body, user.isAdmin).then(result => res.send(result))
})


module.exports = router;
